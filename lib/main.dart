import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import './aspect_ratio.dart';
import './weather.dart';

void main() {
  runApp(const ExampleApp());
}

class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: buildAppBarWidget(),
          body: MyApp(),
        ),
      ),
    );
  }
}

// void main() {
//   runApp(const AspectRatioExample());
// }
//
// class AspectRatioExample extends StatelessWidget {
//   const AspectRatioExample({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         body: Center(
//           child: SizedBox(
//             width: 600.0,
//             child: AspectRatio(
//               aspectRatio: 1.5,
//               child: Container(
//                 color: Colors.green[200],
//                 child: const FlutterLogo(),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }


// void main() {
//   runApp(const AlignExample());
// }
//
// class AlignExample extends StatelessWidget {
//   const AlignExample({Key? key}) : super(key: key);
//
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//     debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         body: Center(
//           child: Container(
//             height: 200.0,
//             width: 600.0,
//             color: Colors.green[200],
//             child: Align(
//               alignment: Alignment.center,
//               child: Container(
//                 color: Colors.green[200],
//                 child: const Text("Align me!"),
//               )
//             )
//           ),
//         ),
//       ),
//     );
//   }
// }

